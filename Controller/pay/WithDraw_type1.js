[//微信提现
    async function( $CANSHU ){
        if($CANSHU['off'] != 2){
            return  false;
        }
        let $db = await db("pay_withdraw");
        let fan = await $db.Where({'id' : $CANSHU['id']}).Update({'off':3});
        if(!fan){
            return  false;
        }
        let  $features = await Kuoplus("pay");
        if(!$features || !$features['configure'] ||  !$features['configure']['微信提现'] || $features['configure']['微信提现']['0'] == ""){
            await $db.Where({'id' : $CANSHU['id']}).Update({'off':3,'msg':"Type Close"});
            return  false;
        } 
        let $APPID  = $features['configure']['微信提现']['0']?$features['configure']['微信提现']['0']:"";
        let $MCHID  = $features['configure']['微信提现']['1']?$features['configure']['微信提现']['1']:"";
        let $APPKEY = $features['configure']['微信提现']['2']?$features['configure']['微信提现']['2']:"";
        let $cert   = $features['configure']['微信提现']['3']?$features['configure']['微信提现']['3']:"";
        let $key    = $features['configure']['微信提现']['4']?$features['configure']['微信提现']['4']:"";
        let $shuju = {
            'mch_appid':  $APPID,
            'mchid':$MCHID,
            'nonce_str':Md5(Mode("Tools").Uuid()),
            'partner_trade_no':$CANSHU['out_trade_no'],
            'openid':$CANSHU['zhanghao'],
            'check_name':'NO_CHECK',
            'amount':$CANSHU['paymoney']*100,
            'desc' :$CANSHU['remarks'] == ''?"提现":$CANSHU['remarks']
        };
      
        let $stringSignTemp = getarray( azpaixu($shuju))+"&key="+$APPKEY;
        $shuju['sign'] = Md5( $stringSignTemp).toUpperCase();
        let $xml = "<xml>\n";
        for(let $k in $shuju){
            let $v =$shuju[$k];
            $xml+='<'+$k+'>'+$v+'</'+$k+'>'+"\n";
        }
        $xml += "</xml>\n";
        let $fan =  await POST("https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers",$xml,{
            formData:null,
            body: $xml,
            agentOptions: {
                cert:$cert,
                key: $key
            },
        });
        $fan = XmlJx($fan);
        if($fan && $fan['xml']){
            let $genggai ={} ;
            if($fan['xml']['result_code'] && $fan['xml']['result_code'] == 'SUCCESS'){
                $genggai = {
                    'off':4,
                    'ctime':Mode("Tools").Time(),
                    'transaction_id':$fan['xml']['payment_no'],
                    'msg':Mode("Tools").Xss($fan['xml']['result_code'])
                };
            }else{
                $genggai = {
                    'off':5,
                    'ctime':Mode("Tools").Time(),
                    'msg':Mode("Tools").Xss($fan['xml']['err_code_des']?$fan['xml']['err_code_des']:$fan['xml']['return_msg'])
                };
            }
            await $db.Where({'id' : $CANSHU['id']}).Update($genggai);
            if($CANSHU['applicationid'] > 0 && Kuo_pay['WithDraw_Notify']){
                for(var ii in $genggai){
                    $CANSHU[ii] = $genggai[ii];
                }
                Kuo_pay['WithDraw_Notify']($CANSHU);
            }
        }
        return true;
    }
]