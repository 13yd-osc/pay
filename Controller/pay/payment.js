[//个人免签接口 payment
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'text/html; charset=UTF-8';
        let $THIS =  this;
        let $features = await Kuoplus(kuo.class);
        let $LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $_POST = kuo.post;
        let $_GET = kuo.get;
        let $method =kuo.Path['0']&&kuo.Path['0'] != ""?kuo.Path['0']:"submit";
        let $filename = kuo.func_;
        
        let $HTTP = ($features['configure'][$filename]?$features['configure'][$filename]['0']:WZHOST); //通信网址
        let $PAYHTTP  = $HTTP+"payment/pay/";
        let $FINDHTTP = $HTTP+"payment/find/";

        let $APPID = $features['configure'][$filename]?$features['configure'][$filename]['1']:"";//通信id
        let $APPKEY = $features['configure'][$filename]?$features['configure'][$filename]['2']:""; //通信私钥
      
        
      
        let $PAYYB = KuoLink(["pay",$filename,"notify"]);
        let $PAYTB = KuoLink(["pay",$filename,"return"]); 

        if($method == 'return'){
            //同步返回
            let uuul = $features['configure']['同步跳转']?$features['configure']['同步跳转']:[];
            Context.statusCode = 302;
            Context.headers["Location"] = KuoLink(uuul)+'?out_trade_no='+$_GET['orderid'];
            Context.body = "";
            return ;
    
        }else if($method == 'notify'){
           
            let $DATA_ ={
                'mchid':$APPID,
                'orderid':$_POST['orderid'],
                'id':$_POST['id'],
                'moeny':$_POST['moeny'],
                'paymoeny':$_POST['paymoeny'],
                'remarks':$_POST['remarks'],
                'off':$_POST['off'],
                'uid':$_POST['uid'],
                'productid':$_POST['productid'],
                'time':$_POST['time'],
                'token':$_POST['token'],
            };
            let $SGIN = $_POST['sign'];
            if($SGIN == Md5($DATA_['mchid']+$DATA_['orderid']+$DATA_['id']+$DATA_['moeny']+$DATA_['paymoeny']+$DATA_['remarks']+$DATA_['off']+$DATA_['uid']+$DATA_['productid']+$DATA_['time']+$DATA_['token']+$APPKEY )){

                let $token = Md5($DATA_['token'])+Md5($SGIN );
                let $sgin = Md5($APPID+$token+$DATA_['orderid']+$APPKEY );
                let $fan = await GET($FINDHTTP+$DATA_['orderid']+"/"+$sgin+"/"+$token +"/");
                $fan = json_decode($fan);
                if($fan && $fan['code'] && $fan['code'] == 1){
                    if( 
                        $DATA_['off'] == 2 &&
                        $fan['data']['moeny']  == $DATA_['moeny'] &&
                        $DATA_['off'] == $fan['data']['off']
                    ){
                        await $THIS.UnifiedBack({
                            'out_trade_no': $DATA_['orderid'],
                            'money':$DATA_['moeny'],
                            'transaction_id':$DATA_['id'],
                            'huomoney':$DATA_['paymoeny']
                        });
                    }
                }
            }
            Context.body = 'success';
            return ;
        }else{
            //提交订单
            if($APPID == "" || $APPKEY == ""){
                $DATA.code = -1;$DATA.msg = $LANG.tong_off;Context.body = JSON.stringify($DATA);
                return ;
            }
            let $ORDER = null;
            let $db =  await db('pay_payment');
            if($method == 'submit'){
                let $Security = await Mem.Get("session/"+kuo.sessionid);
                if(!$Security ){
                    $Security = {'uid':0};
                  
                }
                if(!$Security || !$Security['uid'] ||  $Security['uid'] < 1){
                    $DATA.code = -1;$DATA.msg = $LANG.no_login;Context.body = JSON.stringify($DATA);
                    return ;
                }
                
                let $type = $_GET['type']?$_GET['type']:0;
                
                let $money = $_GET['money']?$_GET['money']:1;
                if($money <= 0){
                    $money = 1;
                }
                $ORDER = await $THIS.PayMent_Add({ 
                    'uid':$Security['uid'],
                    'type': $type ,
                    'money': $money,
                    'plus': kuo.class,
                    'remarks':  $_GET['remarks']?$_GET['remarks']:'' ,
                    'ip':kuo.ip,
                    'agent':kuo.agent
                });
                
            }else{

                let $safe = await Mem.Add("orderid/"+$method,1,$THIS.$findtime );
                if($safe > $THIS.$safenum ){
                    $DATA.code = -1;$DATA.msg = $LANG.shouhoufang;Context.body = JSON.stringify($DATA);
                    return ;
                }
                $ORDER = await $db.Where({'out_trade_no': $method}).Find();
            }

            if(!$ORDER){
                $DATA.code = -1;$DATA.msg = $LANG.add_no;Context.body = JSON.stringify($DATA);
                return ;
            }
            if($ORDER['off'] !=  0){
                $DATA.code = -1;$DATA.msg = $LANG.off_error;Context.body = JSON.stringify($DATA);
                return ;
            }
            await $db.Where({'id': $ORDER['id']}).Update({off:1});

            let $CAN = {
                'orderid': $ORDER['out_trade_no'],
                'type': $ORDER['type'],
                'uid': 0,
                'subject':$ORDER['subject'] == ""?"pay":$ORDER['subject'],
                'notify':$PAYYB,
                'return':$PAYTB,
                'productid': 0,
                'moeny': $ORDER['money'],
                'time':Mode("Tools").Time(),
                'remarks': $ORDER['remarks'],
                'back':'html',
                'mchid': $APPID
            };
            $CAN['sign'] = Md5( $CAN['uid']+$CAN['mchid']+$CAN['type']+$CAN['orderid']+$CAN['moeny'] +$CAN['time'] +$CAN['remarks']+$CAN['subject']+$CAN['productid']+$APPKEY );
            let $sHtml = "<form id='alipaysubmit' name='alipaysubmit' action='"+$PAYHTTP+"' method='post'>";
            for( let $key in $CAN  ) {
                let $val = $CAN[$key];
                $sHtml+= "<input type='hidden' name='"+$key+"' value='"+$val+"'/>";
            }
            $sHtml+="<input type='submit' value='loading..'></form>";
            $sHtml+="<script>document.forms['alipaysubmit'].submit();</script>";
            Context.body = $sHtml;
            return ;
        }
    }
]