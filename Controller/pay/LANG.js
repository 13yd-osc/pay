[//语言包

function(lang){
    if(!lang){
        lang  = Language;
    }
    let YUYAN = {
        "cn":{
           
            "put_ok":"修改成功",
            "put_no":"修改失败",
            "add_ok":"新增成功",
            "add_no":"新增失败",
            "del_ok":"删除成功",
            "del_no":"删除失败",
            "get_no":"不存在",
            "off_error":"状态错误",
            "name_kong":"名字不能为空",
            "cerir_ok":"清理成功",
            "id_renshenhei":"人工审核未开启",
            "id_yesok":"已经处理",
            "tong_off":"通道关闭",
            "no_login":"没有登陆",
            "shouhoufang":"请稍后访问",
            "find_error":"查询错误",
            
            
          


        },
    };
    if(YUYAN[lang]){
        return YUYAN[lang];
    }
    return YUYAN["cn"];
}

]