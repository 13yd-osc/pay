[//支付宝提现
    async function( $CANSHU ){

        if($CANSHU['off'] != 2){
            return  false;
        }
        let $db = await db("pay_withdraw");
        let fan = await $db.Where({'id' : $CANSHU['id']}).Update({'off':3});
        if(!fan){
            return  false;
        }
        let $PAYHTTP  = 'https://openapi.alipay.com/gateway.do?_input_charset=utf-8'; //支付发起地址
        //let $PAYHTTP  = 'https://openapi.alipaydev.com/gateway.do?_input_charset=utf-8'; //支付发起地址
        let  $features = await Kuoplus("pay");
        if(!$features || !$features['configure'] ||  !$features['configure']['支付宝提现'] || $features['configure']['支付宝提现']['0'] == ""){
            await $db.Where({'id' : $CANSHU['id']}).Update({'off':3,'msg':"Type Close"});
            return  false;
        }
        let $APPID  = $features['configure']['支付宝提现']['0'];
        let $APPKEY = $features['configure']['支付宝提现']['1'];
        let $biz_content = {
            'out_biz_no':$CANSHU['out_trade_no'],
            'trans_amount': $CANSHU['paymoney'],
            'product_code':'TRANS_ACCOUNT_NO_PWD',
            'biz_scene':'DIRECT_TRANSFER',
            'payee_info': {
                'identity': $CANSHU['zhanghao'],
                'identity_type':'ALIPAY_LOGON_ID',
                'name': $CANSHU['xingming']
            },'remark': $CANSHU['remarks']
        };
        let $CAN = {
            'app_id' : $APPID,
            'method': 'alipay.fund.trans.uni.transfer',
            'charset':'utf-8',
            'sign_type':'RSA2',
            'timestamp':Mode("Tools").Date("Y-m-d H:i:s"),
            'version':'1.0',
            '_input_charset':'utf-8',
            'biz_content': json_encode($biz_content)
        };
        let $shuju = getarray(azpaixu($CAN));
        $CAN['sign']=  SHA256_sign($shuju,$APPKEY);
        let $fan = await POST( $PAYHTTP ,$CAN);
        $fan  = json_decode($fan);
        if($fan){
            let $fanhui = $fan['alipay_fund_trans_uni_transfer_response'];
            if($fanhui){
                let $genggai ={} ;
                if($fanhui['code'] == '10000'){
                    $genggai = {
                        'off':4,
                        'ctime':Mode("Tools").Time(),
                        'transaction_id':$fanhui['order_id'],
                        'msg':Mode("Tools").Xss($fanhui['msg'])
                    };
                }else{
                    $genggai = {
                        'off':5,
                        'ctime':Mode("Tools").Time(),
                        'msg':Mode("Tools").Xss($fanhui['sub_msg'])
                    };
                }
                await $db.Where({'id' : $CANSHU['id']}).Update($genggai);
                if($CANSHU['applicationid'] > 0 && Kuo_pay['WithDraw_Notify']){
                    for(var ii in $genggai){
                        $CANSHU[ii] = $genggai[ii];
                    }
                    Kuo_pay['WithDraw_Notify']($CANSHU);
                }
            }
        }
        return true;
    }
]