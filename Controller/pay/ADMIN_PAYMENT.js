[//

    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA.token = Mode("Tools").Uuid();
        await  Mem.Set($hash,$DATA.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('pay_payment');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
    
            let $page = Mode("Tools").Int($_GET['page']?$_GET['page']:1);
            let $limitx  =  Mode("Tools").Int($_GET['limit']?$_GET['limit']:10);
            if($limitx < 10 ){
                $limitx = 10;
            }else if($limitx > 100 ){
                $limitx = 100;
            }
            let $where ={};
            if(isset($_GET['off']) && $_GET['off'] != ""){
                $chushi = false; 
                $where['off'] = $_GET['off'];
            }
            if(isset($_GET['type']) && $_GET['type'] != ""){
                $chushi = false; 
                $where['type'] = $_GET['type'];
            }
        
            if(isset($_GET['applicationid']) && $_GET['applicationid'] != ""){
                $chushi = false; 
                $where['applicationid'] = $_GET['applicationid'];
            }
        
            if(isset($_GET['uid']) && $_GET['uid'] != ""){
                $chushi = false; 
                $where['uid'] = $_GET['uid'];
            }
        
            if(isset($_GET['outno']) && $_GET['outno'] != ""){
                $chushi = false; 
                $where['('] = "and";
                $where['out_trade_no'] = $_GET['outno'];
                $where['transaction_id OR'] = $_GET['outno'];
                $where['orderid OR'] = $_GET['outno'];
                $where[')'] = "";
            }
            
        
        
        
        
            if(isset($_GET['atimestart']) && $_GET['atimestart'] != ""){
                $chushi = false; 
                $where['atime >'] =strtotime($_GET['atimestart']);
            }
            if(isset($_GET['atimeend']) && $_GET['atimeend'] != ""){
                $chushi = false; 
                $where['atime <'] =strtotime($_GET['atimeend']);
            }


            let $data  = await $db.Where($where).Limit($limitx,$page).Order("id desc").Select();
            let $total = await $db.Where($where).Total();
            if(!$data){
                $data= [];
            }
            $DATA.code = 0;
            $DATA.count = $total;
            if($page == 1 && $chushi){
                $DATA['off'] = $features['configure']['paymentoff']?$features['configure']['paymentoff']:[];
                $DATA['type'] = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
            }else{
                let $zonge = await $db.Qurey( "select sum(paymoney)as jine from `"+$db.Biao()+"` "+ $db.Wherezuhe($where));
                if($zonge ){
                    $DATA.zonge = $zonge['0']['jine']? $zonge['0']['jine']:0;  
                }
            
            }
    
            $DATA.data = $data;
    
    
        }else if($kongzhi == 'put'){
            
            $DATA.code = -1;
            $DATA.msg = LANG.admin_id_no;
                
            
        }else if($kongzhi == 'add'){
    
     
            $DATA.code = -1;
            $DATA.msg = LANG.add_no;
                
    
        }else if($kongzhi == 'del'){
    
            let $db_ = await db('pay_payment');
            $db_.Where( {'atime <':Mode("Tools").Time()-86400 ,'off IN':'0,1,3' } ).Delete();   
            $DATA.code = 1; 
            $DATA.msg = LANG.cerir_ok;
    
        }
        
        Context.body = JSON.stringify($DATA);
    }
    
]