[//充值订单录入
    async function( $CANSHU ){
        let $uid = $CANSHU['uid']?$CANSHU['uid']:0;
        let $applicationid = $CANSHU['applicationid']?$CANSHU['applicationid']:0;
        if($uid  == 0 && $applicationid){
            return false;
        }
  
        let MONEY = Mode("Tools").Jine($CANSHU['money']?$CANSHU['money']:0);
        if(MONEY < 0){
            MONEY = 1;
        }
      
        let $db = await db('pay_payment');
        let $shuju = {
            'out_trade_no' : orderid(),
            'uid':$uid ,
            'orderid':Mode("Tools").Xss($CANSHU['orderid']?$CANSHU['orderid']:""),
            'type': $CANSHU['type']?$CANSHU['type']:0,
            'applicationid':$applicationid,
            'money': MONEY, 
            'plus':   Mode("Tools").Xss($CANSHU['plus']?$CANSHU['plus']:'pay'),
            'callplus': Mode("Tools").Xss($CANSHU['callplus']?$CANSHU['callplus']:''),
            'body':  Mode("Tools").Xss($CANSHU['body']?$CANSHU['body']:''),
            'subject': Mode("Tools").Xss($CANSHU['subject']?$CANSHU['subject']:''),
            'ip':$CANSHU['ip']?$CANSHU['ip']:"",
            'off':0,
            'anget':Mode("Tools").Xss($CANSHU['agent']?$CANSHU['agent']:""),
            'atime':Mode("Tools").Time(),
            'remarks':Mode("Tools").Xss($CANSHU['remarks']?$CANSHU['remarks']:'')
        };
        if($shuju['money'] <= 0){
            return false;
        }
        
        let $fan = await $db.Insert($shuju );
        if($fan){
            $shuju['id'] = $fan;
            return $shuju;
        }
        return false;

    }
]