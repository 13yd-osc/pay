[//

    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA.token = Mode("Tools").Uuid();
        await  Mem.Set($hash,$DATA.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('pay_withdraw');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
    
            let $page = Mode("Tools").Int($_GET['page']?$_GET['page']:1);
            let $limitx  =  Mode("Tools").Int($_GET['limit']?$_GET['limit']:10);
            if($limitx < 10 ){
                $limitx = 10;
            }else if($limitx > 100 ){
                $limitx = 100;
            }
            let $where ={};
            if(isset($_GET['off']) && $_GET['off'] != ""){
                $chushi = false; 
                $where['off'] = $_GET['off'];
            }
            if(isset($_GET['type']) && $_GET['type'] != ""){
                $chushi = false; 
                $where['type'] = $_GET['type'];
            }
        
            if(isset($_GET['applicationid']) && $_GET['applicationid'] != ""){
                $chushi = false; 
                $where['applicationid'] = $_GET['applicationid'];
            }
        
            if(isset($_GET['uid']) && $_GET['uid'] != ""){
                $chushi = false; 
                $where['uid'] = $_GET['uid'];
            }
        
            if(isset($_GET['outno']) && $_GET['outno'] != ""){
                $chushi = false; 
                $where['('] = "and";
                $where['out_trade_no'] = $_GET['outno'];
                $where['transaction_id OR'] = $_GET['outno'];
                $where['orderid OR'] = $_GET['outno'];
                $where[')'] = "";
            }
        
            if(isset($_GET['atimestart']) && $_GET['atimestart'] != ""){
                $chushi = false; 
                $where['atime >'] =strtotime($_GET['atimestart']);
            }
            if(isset($_GET['atimeend']) && $_GET['atimeend'] != ""){
                $chushi = false; 
                $where['atime <'] =strtotime($_GET['atimeend']);
            }
            let $data  = await $db.Where($where).Limit($limitx,$page).Order("id desc").Select();
            let $total = await $db.Where($where).Total();
            if(!$data){
                $data= [];
            }
            $DATA.code = 0;
            $DATA.count = $total;
            if($page == 1 && $chushi){
                $DATA['off']  = $features['configure']['withdrawoff']?$features['configure']['withdrawoff']:[];
                $DATA['type'] = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
            }
    
            $DATA.data = $data;
    
        }else if($kongzhi == 'put'){
            
        }else if($kongzhi == 'add'){

            let shenhe = $features['configure']['人工审核']?$features['configure']['人工审核']['0']:0;
            if(shenhe != 1){
                $DATA.code = -1;$DATA.msg = LANG.id_renshenhei;Context.body = JSON.stringify($DATA);
                return ;
            }
            
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
            if(!$data){
                $DATA.code = -1;$DATA.msg = LANG.admin_id_no; Context.body = JSON.stringify($DATA);
                return ;
            }
            if($data['off'] != 0 ){
                $DATA.code = -1; $DATA.msg = LANG.id_yesok; Context.body = JSON.stringify($DATA);
                return ;
            }
            let $off = 1;
            let $kongzhi = Mode("Tools").Int($_POST['kongzi']?$_POST['kongzi']:0);
            if($kongzhi == 2){
                $off = 2;
            }else{
                $off = 1;
            }

            let $fan = await $db.Where({id: $id}).Update({off:$off,ctime:Mode("Tools").Time()});
            if($fan){
                await Kuolog('adminlog',kuo.adminid,3,{'yuan':$kongzhi,'data':$off},kuo.func_,kuo.class,kuo.ip);
                $DATA.data = {off:$off};
                $DATA.code =1;
                $DATA.msg = LANG.put_ok;
            }else{
                $DATA.code =-1;
                $DATA.msg = LANG.put_no;
            }

    
        }else if($kongzhi == 'del'){

        }
        Context.body = JSON.stringify($DATA);
    }
    
]