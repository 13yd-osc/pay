[//统一返回
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'application/json; charset=UTF-8';
        let $THIS =  this;
        let $features = await Kuoplus(kuo.class);
        let $LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        
        let $_GET = kuo.get;
        
        let out_trade_no = $_GET['out_trade_no']?$_GET['out_trade_no']:"";
        if(out_trade_no == ""){
            $DATA.code = -1;$DATA.msg = $LANG.find_error;Context.body = JSON.stringify($DATA);
            return ;
        }
        let $db =  await db('pay_payment');
        let data = await $db.Zhicha("off").Where({out_trade_no:out_trade_no}).Find();
        if(!data){
            $DATA.code = -1;$DATA.msg = $LANG.find_error;Context.body = JSON.stringify($DATA);
            return ;
        }
        $DATA.code = data.off;
        $DATA.msg = "";Context.body = JSON.stringify($DATA);
        return ;
        
    }
]