[//统一返回
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'application/json; charset=UTF-8';
        let $THIS =  this;
        let $features = await Kuoplus(kuo.class);
        let $LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $_POST = kuo.post;
        let $_GET = kuo.get;
        let $method =kuo.Path['0']&&kuo.Path['0'] != ""?kuo.Path['0']:"submit";
        let $filename = kuo.func_;
        let $PAYHTTP  = 'https://openapi.alipay.com/gateway.do?_input_charset=utf-8'; //支付发起地址
        let $PAYYB = KuoLink(["pay",$filename,"notify"]);
        let $PAYTB = KuoLink(["pay",$filename,"return"]); 

        let $APPID = $features['configure'][$filename]?$features['configure'][$filename]['0']:""; //公众号ID
        let $MCHID = $features['configure'][$filename]?$features['configure'][$filename]['1']:"";//mchid
        let $APPKEY = $features['configure'][$filename]?$features['configure'][$filename]['2']:""; //私钥
        let $SERIAL  = $features['configure'][$filename]?$features['configure'][$filename]['3']:"";//证书id
        let $V3KEY  = $features['configure'][$filename]?$features['configure'][$filename]['4']:"";//v3 密钥

        if($method == 'return'){
            //同步返回
            let uuul = $features['configure']['同步跳转']?$features['configure']['同步跳转']:[];
            Context.statusCode = 302;
            Context.headers["Location"] = KuoLink(uuul)+'?out_trade_no='+$_GET['out_trade_no'];
            Context.body = "";
            return ;
    
        }else if($method == 'notify'){
           


            Context.body = 'success';
          
            

            
            Context.body = 'fail';
            return ;

        }else{
            //提交订单
            if($APPID == "" || $APPKEY == ""){
                $DATA.code = -1;$DATA.msg = $LANG.tong_off;Context.body = JSON.stringify($DATA);
                return ;
            }
            let $ORDER = null;
            let $db =  await db('pay_payment');
            if($method == 'submit'){
                let $Security = await Mem.Get("session/"+kuo.sessionid);
                if(!$Security ){
                    $Security = {'uid':0};
                  
                }
                if(!$Security || !$Security['uid'] ||  $Security['uid'] < 1){
                    $DATA.code = -1;$DATA.msg = $LANG.no_login;Context.body = JSON.stringify($DATA);
                    return ;
                }
                
                let $type = 0;
                
                let $money = $_GET['money']?$_GET['money']:1;
                if($money <= 0){
                    $money = 1;
                }
                $ORDER = await $THIS.PayMent_Add({ 
                    'uid':$Security['uid'],
                    'type': $type ,
                    'money': $money,
                    'plus': kuo.class,
                    'remarks':  $_GET['remarks']?$_GET['remarks']:'' ,
                    'ip':kuo.ip,
                    'agent':kuo.agent
                });
                
            }else{

                let $safe = await Mem.Add("orderid/"+$method,1,$THIS.$findtime );
                if($safe > $THIS.$safenum ){
                    $DATA.code = -1;$DATA.msg = $LANG.shouhoufang;Context.body = JSON.stringify($DATA);
                    return ;
                }
                $ORDER = await $db.Where({'out_trade_no': $method}).Find();
            }

            if(!$ORDER){
                $DATA.code = -1;$DATA.msg = $LANG.add_no;Context.body = JSON.stringify($DATA);
                return ;
            }
            if($ORDER['off'] !=  0){
                $DATA.code = -1;$DATA.msg = $LANG.off_error;Context.body = JSON.stringify($DATA);
                return ;
            }
            await $db.Where({'id': $ORDER['id']}).Update({off:1});






            Context.body = "";
            return ;
        }
    }
]