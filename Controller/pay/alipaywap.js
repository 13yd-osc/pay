[//统一返回
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'application/json; charset=UTF-8';
        let $THIS =  this;
        let $features = await Kuoplus(kuo.class);
        let $LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $_POST = kuo.post;
        let $_GET = kuo.get;
        let $method =kuo.Path['0']&&kuo.Path['0'] != ""?kuo.Path['0']:"submit";
        let $filename = kuo.func_;
        let $PAYHTTP  = 'https://openapi.alipay.com/gateway.do?_input_charset=utf-8'; //支付发起地址
        let $PAYYB = KuoLink(["pay",$filename,"notify"]);
        let $PAYTB = KuoLink(["pay",$filename,"return"]); 
        let $APPID =  $features['configure'][$filename]?$features['configure'][$filename]['0']:""; //应用ID
        let $APPKEY = $features['configure'][$filename]?$features['configure'][$filename]['1']:""; //应用私钥
        let $ALIKEY = $features['configure'][$filename]?$features['configure'][$filename]['2']:""; //应用公钥
        if($method == 'return'){
            //同步返回
            let uuul = $features['configure']['同步跳转']?$features['configure']['同步跳转']:[];
            Context.statusCode = 302;
            Context.headers["Location"] = KuoLink(uuul)+'?out_trade_no='+$_GET['out_trade_no'];
            Context.body = "";
            return ;
    
        }else if($method == 'notify'){
            //异步返回
            let $sign = $_POST['sign']?$_POST['sign']:"";
            let $sign_type = $_POST['sign_type']?$_POST['sign_type']:"";
            if($sign == "" ||  $sign_type == ""){
                Context.body = 'fail';
                return ;
            }
            delete($_POST['sign']);
            delete($_POST['sign_type']);
            let $shuju = getarray(azpaixu($_POST));
            if(SHA256_verify($shuju, $sign, $ALIKEY)){

                let $biz_content = {
                    'out_trade_no':$_POST['out_trade_no'],
                    'trade_no':$_POST['trade_no']
                };
                let $CAN = {
                    'app_id' : $APPID,
                    'method': 'alipay.trade.query',
                    'charset':'utf-8',
                    'sign_type':'RSA2',
                    'timestamp':Mode("Tools").Date("Y-m-d H:i:s"),
                    'version':'1.0',
                    '_input_charset':'utf-8',
                    'biz_content': json_encode($biz_content),   
                };
                let $shuju = getarray(azpaixu($CAN));
                $CAN['sign']=  SHA256_sign($shuju,$APPKEY);
                let $fan = await POST( $PAYHTTP ,$CAN);
                $fan  = json_decode($fan);
                if($fan && $fan['alipay_trade_query_response'] && $_POST['trade_status']  == "TRADE_SUCCESS"){
                    let $fanhui = $fan['alipay_trade_query_response'];
                    if($fanhui['code'] == "10000" && $_POST['total_amount'] == $fanhui['total_amount']){
                        await $THIS.UnifiedBack({
                            'out_trade_no': $_POST['out_trade_no'],
                            'money':$_POST['total_amount'],
                            'transaction_id':$_POST['trade_no'],
                            'huomoney':$_POST['total_amount']
                        });
                    }
                }
                Context.body = 'success';
                return ;
            }
            
            Context.body = 'fail';
            return ;

        }else{
            //提交订单
            if($APPID == "" || $APPKEY == ""){
                $DATA.code = -1;$DATA.msg = $LANG.tong_off;Context.body = JSON.stringify($DATA);
                return ;
            }
            let $ORDER = null;
            let $db =  await db('pay_payment');
            if($method == 'submit'){
                let $Security = await Mem.Get("session/"+kuo.sessionid);
                if(!$Security ){
                    $Security = {'uid':0};
                  
                }
                if(!$Security || !$Security['uid'] ||  $Security['uid'] < 1){
                    $DATA.code = -1;$DATA.msg = $LANG.no_login;Context.body = JSON.stringify($DATA);
                    return ;
                }
                
                let $type = 0;
                
                let $money = $_GET['money']?$_GET['money']:1;
                if($money <= 0){
                    $money = 1;
                }
                $ORDER = await $THIS.PayMent_Add({ 
                    'uid':$Security['uid'],
                    'type': $type ,
                    'money': $money,
                    'plus': kuo.class,
                    'remarks':  $_GET['remarks']?$_GET['remarks']:'' ,
                    'ip':kuo.ip,
                    'agent':kuo.agent
                });
                
            }else{

                let $safe = await Mem.Add("orderid/"+$method,1,$THIS.$findtime );
                if($safe > $THIS.$safenum ){
                    $DATA.code = -1;$DATA.msg = $LANG.shouhoufang;Context.body = JSON.stringify($DATA);
                    return ;
                }
                $ORDER = await $db.Where({'out_trade_no': $method}).Find();
            }

            if(!$ORDER){
                $DATA.code = -1;$DATA.msg = $LANG.add_no;Context.body = JSON.stringify($DATA);
                return ;
            }
            if($ORDER['off'] !=  0){
                $DATA.code = -1;$DATA.msg = $LANG.off_error;Context.body = JSON.stringify($DATA);
                return ;
            }
            await $db.Where({'id': $ORDER['id']}).Update({off:1});
            let $biz_content = {
                'product_code': 'FAST_INSTANT_TRADE_PAY',
                'out_trade_no':$ORDER['out_trade_no'],
                'total_amount':$ORDER['money'],
                'subject':$ORDER['subject'] == ""?"pay":$ORDER['subject'],
                'body': $ORDER['body'] == ""? "支付" :$ORDER['body'],
                passback_params:$ORDER['applicationid']
            };
            let $CAN = {
                'app_id' : $APPID,
                'method': 'alipay.trade.wap.pay',
                'charset':'utf-8',
                'sign_type':'RSA2',
                'timestamp':Mode("Tools").Date("Y-m-d H:i:s"),
                'version':'1.0',
                'notify_url':$PAYYB,
                '_input_charset':'utf-8',
                'biz_content': json_encode($biz_content),
                'return_url':$PAYTB,
                
            };
            let $shuju = getarray(azpaixu($CAN));
            $CAN['sign']=  SHA256_sign($shuju,$APPKEY);
            Context.headers["content-type"] = 'text/html; charset=UTF-8';
            let $sHtml = "<form id='alipaysubmit' name='alipaysubmit' action='"+$PAYHTTP+"' method='post'>";
            for( let $key in $CAN  ) {
                let $val = $CAN[$key];
                $sHtml+= "<input type='hidden' name='"+$key+"' value='"+$val+"'/>";
            }
            $sHtml+="<input type='submit' value='loading..'></form>";
            $sHtml+="<script>document.forms['alipaysubmit'].submit();</script>";
            
            Context.body = $sHtml;
            return ;
        }
    }
]