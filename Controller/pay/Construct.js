[//

async function( kuo,Context,context){
    let func_ = kuo.func_.toUpperCase();
   
    Context.headers["content-type"] = 'application/json; charset=UTF-8';
    let $DATA = Object.assign({},this.$DATA);
    let $Security = await Mem.Get("session/"+kuo.sessionid);
    if(! $Security ){
        $Security ={ adminid: 0};
    }
    if(typeof($Security['adminid']) == "undefined" ){
        $Security['adminid'] = 0;
    }
    if($Security['adminid'] < 1){
        $DATA.code = 99;
        $DATA.msg = "Go Login";
        Context.body =JSON.stringify($DATA);
        return ;
    }
    let fan = await Mode("admin").Loginok(kuo,$Security);
    if(fan){
        $DATA.code = fan;
        $DATA.msg = "Error";
        if($Security['adminid'] > 0){
            let $hash = 'safetoken/'+$Security['adminid'];
            $DATA.token = Mode("Tools").Uuid();
            Mem.Set($hash,$DATA.token);
        }
        Context.body = JSON.stringify($DATA);
        return ;
    }

    if(this[func_]){  
        this.$features = await Kuoplus(kuo.class);    
        kuo.adminid = $Security['adminid'];
        await this[func_](kuo,Context,context);
        return "";
    }
    $DATA.code = -1;
    $DATA.msg ="Not "+func_;
    Context.body = JSON.stringify($DATA);
}

]