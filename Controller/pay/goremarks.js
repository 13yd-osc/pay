[//remarks 跳转
    async function( kuo,Context,context){
        let $THIS =  this;
        let $features = await Kuoplus(kuo.class);
        let $LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $_POST = kuo.post;
        let $_GET = kuo.get;
        let $out_trade_no = $_GET['out_trade_no'];
        if(!$out_trade_no){
            let $db = await db('pay_payment');
            let $DINGDAN =await  $db.Where({'out_trade_no': $out_trade_no }).Find();
            if($DINGDAN){
                if($DINGDAN['remarks'] != "" && $DINGDAN['remarks'].indexOf("http") >-1){
                    Context.statusCode = 302;
                    Context.headers["Location"] = decodeURIComponent($DINGDAN['remarks']);
                    Context.body = "";
                    return ;
                }

            }

        }
        Context.body = $out_trade_no;
        return ;

    }
]