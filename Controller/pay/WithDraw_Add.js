[//提现订单录入
    async function( $CANSHU ){
        let $db = await db('pay_withdraw');
        let MONEY = Mode("Tools").Jine($CANSHU['money']?$CANSHU['money']:0);
        
        let bili = $CANSHU['bili']?$CANSHU['bili']:1;
        if(!$CANSHU['bili']){
            let  $features = await Kuoplus("pay");
            if($features && $features['configure']['提现比例']&& $features['configure']['提现比例']['0']){
                bili = $features['configure']['提现比例']['0'];
                if(bili >= 1){
                    bili = 1;
                }
            }
        }
        let $MONEY = Mode("Tools").Jine(MONEY*bili);
        if(MONEY < 0){
            MONEY = 1;
        }
        let $shuju = {
            'out_trade_no' : orderid(),
            'orderid':Mode("Tools").Xss($CANSHU['orderid']),
            'type':$CANSHU['type']?$CANSHU['type']:0,
            'applicationid':$CANSHU['applicationid']?$CANSHU['applicationid']:0,
            'uid':$CANSHU['uid']?$CANSHU['uid']:0,
            'money':MONEY,
            'paymoney':$MONEY ,
            'atime':Mode("Tools").Time(),
            'ip':$CANSHU['ip']?$CANSHU['ip']:"",
            'off':0,
            'riqi':Mode("Tools").Date('Ymd'),
            'xingming':Mode("Tools").Xss($CANSHU['xingming']?$CANSHU['xingming']:''),
            'zhanghao':Mode("Tools").Xss($CANSHU['zhanghao']?$CANSHU['zhanghao']:''),
            'yinghang':Mode("Tools").Xss($CANSHU['yinghang']?$CANSHU['yinghang']:''),
            'plus':Mode("Tools").Xss($CANSHU['plus']?$CANSHU['plus']:'pay'),
            'anget':Mode("Tools").Xss($CANSHU['anget']?$CANSHU['anget']:""),
            'remarks':Mode("Tools").Xss($CANSHU['remarks']?$CANSHU['remarks']:"")
        };
        if($shuju['money'] <= 0 || $shuju['zhanghao'] == ""){
            return false;
        }
        let $fan = await $db.Insert($shuju );
        if($fan){
            $shuju['id'] = $fan;
            return $shuju;
        }
        return false;
    }
]