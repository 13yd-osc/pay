插件名称：支付系统   
系统名称：阔老板  
官方网址：[https://www.kuosoft.com](https://www.kuosoft.com)  
开源代码协议 Apache License 2.0 详见 http://www.apache.org/licenses/   
## 插件说明 

微信支付V3   
微信零钱到账  
支付宝支付   
支付宝提现  

## 安装 

对应放入文件   
执行 node build.js br  
登陆管理后台   
管理后台 -》 插件管理 -》  输入 pay 立即安装     
前台页面 /pay/ 
插件管理 进行参数配置  

## 插件变量和函数
payment  个人免签接口  
alipayapp 支付宝app  
alipaypc  支付宝PC  
alipaywap 支付宝wap  
   
weixinapp 微信app  
weixinjs 微信公众号   
weixinpc 微信扫码  
weixinwap 微信wap  
weixinxiaog 微信小游戏  
weixinxiaox  微信小程序   
  
PayMent_Add 充值订单录入 
WithDraw_Add 提现订单录入 
WithDraw_Notify 第三方通知(备用)   
WithDraw_type0 支付宝提现逻辑  
WithDraw_type0 微信提现逻辑   

UnifiedBack 统一返回处理  
Run shell 脚本 提现处理  