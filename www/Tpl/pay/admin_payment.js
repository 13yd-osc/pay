Kuo['pay/admin_payment'] = {
    url: plug + "pay" + FENGE + "admin_payment" + FENGE,
    type:[],
    init: function () {
        Kuo['pay/admin_payment'].get();
    },edit(OBJ, diercai) {
        if (diercai != "add") {
            diercai = "put";
            var title = "查看订单";
        } else {
            var title = "新建用户";
        }
        window.UIMUI = Array();
        var D = OBJ.data;
        var BIAO = [];
        var html = '<form name="form" id="form" class="layui-form ' + $class + '">';
        BIAO = [
            'id($$)id($$)hidden($$)($$)id($$)' + D.id,
            'out_trade_no($$)默认订单($$)text($$)($$)默认订单($$)' + D.out_trade_no,
            'transaction_id($$)商户订单($$)text($$)($$)商户订单($$)' + D.transaction_id,
            'orderid($$)应用订单($$)text($$)($$)应用订单($$)' + D.orderid,
            'money($$)充值金额($$)text($$)($$)充值金额($$)' + D.money,
            'paymoney($$)返回金额($$)text($$)($$)返回金额($$)' + D.paymoney,
            'huomoney($$)金额($$)text($$)($$)金额($$)' + D.huomoney,
            'type($$)支付类型($$)selectshow($$)($$)Kuo["pay/admin_payment"].type($$)' + D.type,
            'plus($$)处理函数($$)text($$)($$)处理函数($$)' + D.plus,
            'off($$)订单状态($$)selectshow($$)($$)Kuo["pay/admin_payment"].off($$)' + D.off,
            'uid($$)用户uid($$)text($$)($$)用户uid($$)' + D.uid,
            'applicationid($$)应用id($$)text($$)($$)应用id($$)' + D.applicationid,
            'callplus($$)调用插件($$)text($$)($$)调用插件($$)' + D.callplus,
            'remarks($$)系统备注($$)text($$)($$)系统备注($$)' + D.remarks,
            'subject($$)订单标题($$)text($$)($$)订单标题($$)' + D.subject,
            'body($$)订单描述($$)textarea($$)($$)订单描述($$)' + D.body,
            'anget($$)浏览器($$)textarea($$)($$)浏览器信息($$)' + D.anget,
            'ip($$)ip($$)text($$)($$)ip($$)' + D.ip,
            'atime($$)插入时间($$)time($$)($$)ip($$)' + D.atime,
            'ctime($$)修改时间($$)time($$)($$)ip($$)' + D.ctime
            
        ];

        for (var z in BIAO) {
            html += jsfrom(BIAO[z]);
        }

        html += jsfrom('ff' + $class + '($$)($$)submit($$)($$)layer.close(OPID);Kuo[\'pay/admin_payment\'].init()($$)提交($$)tijiao');
        html += "</form>";
        OPID = layer.open({
            type: 1,
            zIndex: 10000,
            title: title,
            area: ['100%', '100%'],
            fixed: true,
            maxmin: true,
            content: html,
            success: function (layero, index) {
            }
        });

        layui.form.on('submit(ff' + $class + ')', function (formdata) {
            
            layer.close(OPID);
                   
        
            return false;
        });

        layui.form.render();

    },
    get() {
        $("#LAY_app_body ." + $class).html('<style> .' + $class + ' .qfys0{color:#FF5722;} .' + $class + ' .qfys1{color:#009688;}.' + $class + ' .qfys2{color:green;}.' + $class + ' .qfys3{color:#1E9FFF;}</style><div class="layui-fluid"><div class="layui-card"><div class="layui-card-body" style="padding: 15px;">'
            +
            '<div class="' + $class + 'saixuan" style="display:none;margin-bottom:8px;"><form name="form" class="layui-form"> <div class="layui-inline" style="width:128px;"><select name="so_type" class="so_var"></select> </div> <div class="layui-inline" style="width:128px;"><select name="so_off" class="so_var"></select> </div>  <div class="layui-inline" style="width:188px;"> <input class="layui-input so_var" placeholder="搜索订单编号"  name="so_outno" autocomplete="off"> </div> <div class="layui-inline" style="width:88px;"> <input class="layui-input so_var" placeholder="uid"  name="so_uid" autocomplete="off"> </div> <div class="layui-inline" style="width:88px;"> <input class="layui-input so_var" placeholder="应用id"  name="so_applicationid" autocomplete="off"> </div>  <div class="layui-inline" style="width:88px;"> <input class="layui-input so_var" placeholder="开始时间" id="so_atimestart' + $class + '" name="so_atimestart" autocomplete="off"> </div> <div class="layui-inline" style="width:88px;"> <input class="layui-input so_var" placeholder="结束时间" id="so_atimeend' + $class + '" name="so_atimeend" autocomplete="off"> </div>   <button class="layui-btn" lay-event="sousuo" lay-submit lay-filter="tijiao' + $class + '">搜索</button>   <button class="layui-btn layui-btn-danger aabb' + $class + '">0</button></form> </div>'
            +
            '<table class="layui-hide" id="user' + $class + '" lay-filter="user' + $class + '"></table></div></div></div>');


        layui.table.render({
            elem: '#user' + $class,
            url: Kuo['pay/admin_payment'].url + TOKEN + FENGE + 'get',
            toolbar: '<div> <a class="layui-btn layui-btn-danger" lay-event="saixuan"><i class="layui-icon layui-icon-search"></i>筛选</a> <a class="layui-btn layui-btn-normal" lay-event="qingli"><i class="layui-icon layui-icon-refresh-3"></i>清理过期</a>  <a class="layui-btn " lay-event="refresh"><i class="layui-icon layui-icon-refresh-3"></i> 刷新</a>  </div>',
            cols: [
                [{
                    field: 'id',
                    title: 'ID',
                    width: 100,
                    fixed: true
                }, {
                    field: "uid",
                    width: 100,
                    title: "用户uid"
                }, {
                    field: "applicationid",
                    width: 100,
                    title: "应用id"
                }, {
                    field: "out_trade_no",
                    title: "系统订单"
                }, {
                    width: 158,
                    field: "money",
                    title: "金额",
                    templet: function (d) {

                        return d.money +' <span class="qfys' + d.off + '">'+d.paymoney+ '</span>';
                    }
                }, {
                    field: "type",
                    width: 88,
                    title: "类型",
                    templet: function (d) {

                        return Kuo['pay/admin_payment'].type[d.type];
                    }
                }, {
                    field: "off",
                    width: 88,
                    title: "状态",
                    templet: function (d) {

                        return '<span class="qfys' + d.off + '">' + Kuo['pay/admin_payment'].off[d.off] + '</span>';
                    }
                }, {
                    field: "atime",
                    title: "添加时间",
                    templet: function (d) {

                        return time(d.atime);
                    }
                }, {

                    title: '操作',
                    width: 88,
                    templet: function (d) {
                        return '<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>查看</a>';
                    }
                }]
            ],
            limit: 20,
            page: true,
            done: function (data, curr, count) {
                if (data.token && data.token != "") {
                    TOKEN = data.token;
                }

                if(data.zonge && data.zonge != "0"){
                    $(".aabb" + $class).show();
                    $(".aabb" + $class).html(data.zonge);

                }else{
                    $(".aabb" + $class).hide();
                }

                if (data.off) {
                    Kuo['pay/admin_payment'].off = data.off;
                    Kuo['pay/admin_payment'].type = data.type;
                    
                    layui.laydate.render({
                        elem: '#so_atimestart' + $class,
                        format: 'yyyy-MM-dd' //可任意组合
                    });

                    layui.laydate.render({
                        elem: '#so_atimeend' + $class,
                        format: 'yyyy-MM-dd' //可任意组合
                    });


                    var xxx = '<option value="">全部状态</option>';
                    for (var n in data.off) {
                        xxx += '<option value="' + n + '">' + data.off[n] + '</option>';
                    }
                    $('.' + $class + ' [name="so_off"]').html(xxx);

                    var xxx = '<option value="">全部方式</option>';
                    for (var n in data.type) {
                        xxx += '<option value="' + n + '">' + data.type[n] + '</option>';
                    }
                    $('.' + $class + ' [name="so_type"]').html(xxx);

                    layui.form.render();
                }  
            }
        });
        layui.table.on('toolbar(user' + $class + ')', function (obj) {
            if (obj.event === 'qingli') {

                apptongxin(Kuo['pay/admin_payment'].url + TOKEN + FENGE + "del", {}, function (data) {
                    if (data.token && data.token != "") {
                        TOKEN = data.token;
                    }
                    if (data.code == 99) {
                        layer.close(OPID);
                        layer.msg("请登陆", {
                            offset: 'c',
                            time: 2000
                        }, function () {
                            loadjs("home");
                        });

                    }else if (data.code == 1) {
                
                        
                        Kuo['pay/admin_payment'].get();
                           
                    } else {
                
                        layer.msg(data.msg, {
                            zIndex: 99999,
                            offset: 'c',
                            time: 2000
                        });
                    }
                });

            

               
            } else if (obj.event === 'refresh') {
                layer.closeAll();
                Kuo['pay/admin_payment'].init();
            } else if (obj.event === 'saixuan') {
                $("." + $class + "saixuan").toggle();
            }
        });

        layui.table.on('tool(user' + $class + ')', function (obj) {
            if (obj.event === 'edit') {
                Kuo['pay/admin_payment'].edit(obj);
            }
        });

        layui.form.on('submit(tijiao' + $class + ')', function (formdata) {
            formdata = formdata.field;
            var zuhesou = {};
            $('.' + $class + 'saixuan .so_var').each(function (i, v) {
                if ($(v).val() != "") {
                    zuhesou[$(v).attr('name').replace('so_', '')] = $(v).val();
                }
            });
            layui.table.reload('user' + $class, {
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                url: Kuo['pay/admin_payment'].url + TOKEN + FENGE + 'get',
                where: zuhesou
            });
            return false;
        });

        layui.table.render();
    }
}
Kuo['pay/admin_payment'].init();